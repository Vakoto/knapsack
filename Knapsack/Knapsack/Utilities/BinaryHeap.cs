﻿using System;
using System.Collections.Generic;

namespace Knapsack.Utilities
{
    public class BinaryHeap<T>
        where T : IComparable
    {
        private readonly List<T> items;

        public int Count => items.Count;

        public BinaryHeap()
        {
            items = new List<T>();
        }

        private void HeapifyDown(int i)
        {
            while (true)
            {
                var left = 2 * i + 1;
                var right = 2 * i + 2;

                var largest = i;
                if (left < Count && items[left].CompareTo(items[largest]) > 0)
                {
                    largest = left;
                }

                if (right < Count && items[right].CompareTo(items[largest]) > 0)
                {
                    largest = right;
                }

                if (largest == i)
                {
                    break;
                }

                T t = items[i];
                items[i] = items[largest];
                items[largest] = t;

                i = largest;
            }
        }

        private void HeapifyUp(int i, T value)
        {
            if (value.CompareTo(items[i]) < 0)
            {
                throw new ArgumentException(nameof(value));
            }

            items[i] = value;

            while (i > 0)
            {
                var parentIndex = (i - 1) / 2;

                if (items[parentIndex].CompareTo(items[i]) >= 0)
                {
                    break;
                }

                var t = items[parentIndex];
                items[parentIndex] = items[i];
                items[i] = t;

                i = parentIndex;
            }
        }

        public void Add(T value)
        {
            items.Add(value);
            HeapifyUp(Count - 1, value);
        }

        public void AddRange(T[] source)
        {
            foreach (var item in source)
            {
                Add(item);
            }
        }

        public T PeekMax()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException(nameof(Count));
            }

            return items[0];
        }

        public T PopMax()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException(nameof(Count));
            }

            var maxValue = PeekMax();
            items[0] = items[Count - 1];
            items.RemoveAt(Count - 1);
            HeapifyDown(0);

            return maxValue;
        }
    }
}
