﻿using System;
using System.Collections.Generic;
using System.Linq;
using Knapsack.Utilities;

namespace Knapsack.KnapsackSolvers
{
    public class BranchAndBoundSolver : IKnapsackSolver
    {
        private class KnapsackSubset : IComparable
        {
            public int TotalWeight { get; }

            public int TotalProfit { get; }

            public List<int> ShouldTake { get; }

            public double ObjectiveValue { get; }

            public int Height { get; }

            public KnapsackSubset(int totalWeight, int totalProfit, List<int> shouldTake, int height, int capacity, double nextItemCost)
            {
                TotalWeight = totalWeight;
                TotalProfit = totalProfit;
                ShouldTake = shouldTake.ToList();
                Height = height;
                ObjectiveValue = CalculateObjectiveValue(totalProfit, totalWeight, capacity, nextItemCost);
            }

            private double CalculateObjectiveValue(int totalProfit, int totalWeight, int capacity, double nextItemCost)
            {
                return totalWeight <= capacity ? totalProfit + (capacity - totalWeight) * nextItemCost : -1;
            }

            public int CompareTo(object obj)
            {
                if (obj is KnapsackSubset candidate)
                {
                    return ObjectiveValue.CompareTo(candidate.ObjectiveValue);
                }

                throw new ArgumentException(nameof(obj));
            }
        }

        public bool[] Solve(int[] profits, int[] weights, int capacity)
        {
            var heap = new BinaryHeap<KnapsackSubset>();

            var items = Enumerable.Range(0, profits.Length)
                .Select(i => new { Weight = weights[i], Profit = profits[i], Cost = (double)profits[i] / weights[i], Index = i })
                .OrderByDescending(item => item.Cost)
                .ToList();

            heap.Add(new KnapsackSubset(0, 0, new List<int>(), -1, capacity, 0));

            KnapsackSubset currentSubset;

            while (true)
            {
                currentSubset = heap.PopMax();

                var currentHeight = currentSubset.Height + 1;

                if (currentHeight == items.Count)
                {
                    break;
                }
                
                var currentItem = items[currentHeight];
                var nextItemCost = currentHeight + 1 < items.Count ? items[currentHeight + 1].Cost : 0;

                heap.Add(new KnapsackSubset(
                    currentSubset.TotalWeight,
                    currentSubset.TotalProfit,
                    currentSubset.ShouldTake,
                    currentHeight,
                    capacity,
                    nextItemCost)
                );
                
                currentSubset.ShouldTake.Add(currentItem.Index);

                heap.Add(new KnapsackSubset(
                    currentSubset.TotalWeight + currentItem.Weight,
                    currentSubset.TotalProfit + currentItem.Profit,
                    currentSubset.ShouldTake,
                    currentHeight,
                    capacity,
                    nextItemCost
                ));
            }
            
            var answer = new bool[items.Count];

            foreach (var itemIndex in currentSubset.ShouldTake)
            {
                answer[itemIndex] = true;
            }

            return answer;
        }
    }
}
