﻿namespace Knapsack.KnapsackSolvers
{
    public interface IKnapsackSolver
    {
        bool[] Solve(int[] profits, int[] weights, int capacity);
    }
}