﻿using System.Linq;

namespace Knapsack.KnapsackSolvers
{
    public class BruteForceSolver : IKnapsackSolver
    {
        public bool[] Solve(int[] profits, int[] weights, int capacity)
        {
            var n = profits.Length;
            var subset = new bool[n];
            var bestSubset = new bool[n];
            
            var currentCost = 0;
            var bestCost = 0;
            var currentVolume = 0;
           
            var i = 0;
            var b = new int[n + 1];
            for (; i <= n; i++)
            {
                b[i] = i + 1;
            }

            while ((i = b[0]) <= n)
            {
                subset[i - 1] = !subset[i - 1];

                var shouldTake = subset[i - 1] ? 1 : -1;
                currentCost += profits[i - 1] * shouldTake;
                currentVolume += weights[i - 1] * shouldTake;

                if (currentVolume <= capacity && currentCost > bestCost)
                {
                    bestCost = currentCost;
                    bestSubset = subset.ToArray();
                }

                b[0] = 1;
                b[i - 1] = b[i];
                b[i] = i + 1;
            }

            return bestSubset;
        }
    }
}