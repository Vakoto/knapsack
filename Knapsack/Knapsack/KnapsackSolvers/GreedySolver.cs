﻿using System.Linq;

namespace Knapsack.KnapsackSolvers
{
    public class GreedySolver : IKnapsackSolver
    { 
        public bool[] Solve(int[] profits, int[] weights, int capacity)
        {
            var items = Enumerable.Range(0, profits.Length)
                .Select(i => new { Index = i, Weight = weights[i], Cost = profits[i] })
                .OrderByDescending(item => (double)item.Cost / item.Weight)
                .ToList();

            var currentCapacity = 0;
            var answer = new bool[profits.Length];

            foreach (var item in items)
            {
                if (currentCapacity + item.Weight <= capacity)
                {
                    currentCapacity += item.Weight;
                    answer[item.Index] = true;
                }
            }

            return answer;
        }
    }
}