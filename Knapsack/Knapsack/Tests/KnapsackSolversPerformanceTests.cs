﻿using Knapsack.KnapsackSolvers;
using NUnit.Framework;

namespace Knapsack.Tests
{
    [TestFixture]
    public class KnapsackSolversPerformanceTests
    {
        // [Test, Timeout(10000)]
        public static void PerformanceTest([Range(1, 50)]int volume)
        {
            var solver = new BruteForceSolver();
            var profits = new int[volume];
            var weights = new int[volume];
            solver.Solve(profits, weights, int.MaxValue);
        }
    }
}