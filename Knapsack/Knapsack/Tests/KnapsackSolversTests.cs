﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Knapsack.KnapsackSolvers;
using NUnit.Framework;

namespace Knapsack.Tests
{
    [TestFixture]
    public class KnapsackSolversTests
    {
        private static string GetTestDataPath(string fileName)
        {
            var startupPath = AppDomain.CurrentDomain.BaseDirectory;
            var pathItems = startupPath.Split(Path.DirectorySeparatorChar);
            var pos = pathItems.Reverse().ToList().FindIndex(x => string.Equals("bin", x));
            var projectPath = string.Join(Path.DirectorySeparatorChar.ToString(), pathItems.Take(pathItems.Length - pos - 1));
            return Path.Combine(projectPath, "Tests", "TestData", fileName);
        }

        private static int ReadCapacity(string capacityFileName)
        {
            var capacityPath = GetTestDataPath(capacityFileName);
            return int.Parse(File.ReadAllText(capacityPath));
        }

        private static int[] ReadIntegerArray(string fileName)
        {
            var path = GetTestDataPath(fileName);
            return File.ReadLines(path).Select(int.Parse).ToArray();
        }
        
        [Test, TestCaseSource(typeof(KnapsackTestsFactory), nameof(KnapsackTestsFactory.TestCases))]
        public void Test(
            string capacityFileName, string weightsFileName, string profitsFileName, string answersFileName, IKnapsackSolver solver)
        {
            var capacity = ReadCapacity(capacityFileName);
            var weights = ReadIntegerArray(weightsFileName).ToArray();
            var profits = ReadIntegerArray(profitsFileName).ToArray();
            var expected =  ReadIntegerArray(answersFileName).Select(i => i == 1).ToArray();

            var actual = solver.Solve(profits, weights, capacity);

            CollectionAssert.AreEqual(expected, actual);
        }
    }

    internal class KnapsackTestsFactory
    {
        internal static IEnumerable<TestCaseData> TestCases
        {
            get
            {
                foreach (var solver in new IKnapsackSolver[] { new BranchAndBoundSolver(), new BruteForceSolver() })
                {
                    for (var i = 1; i <= 8; i++)
                    {
                        yield return new TestCaseData($"p0{i}_c.txt", $"p0{i}_w.txt", $"p0{i}_p.txt", $"p0{i}_s.txt", solver);
                    }
                }
            }
        }
    }
}
