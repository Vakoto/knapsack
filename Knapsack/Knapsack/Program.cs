﻿using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using Knapsack.KnapsackSolvers;

namespace Knapsack
{
    class Program
    {
        private static double MeasureTime(
            IKnapsackSolver solver,
            int repetitions,
            int[] profits,
            int[] weights,
            int capacity)
        {
            solver.Solve(new int[0], new int[0], 0);
            var sw = new Stopwatch();

            sw.Start();
            for (int i = 0; i < repetitions; i++)
            {
                solver.Solve(profits, weights, capacity);
            }
            sw.Stop();
            GC.Collect();

            return sw.ElapsedTicks / repetitions;
        }

        private const int vMax = 29;

        private static Random random = new Random();

        private static int[] GenerateRandomArray(int size, int maxValue)
        {
            var result = new int[size];
            for (var i = 0; i < size; i++)
            {
                result[i] = random.Next(0, maxValue + 1);
            }
            return result;
        }

        private static void PerformanceTest()
        {
            var bruteForceSolver = new BruteForceSolver();
            var greedySolver = new GreedySolver();
            var count = 100;
            var maxValue = 50;
            var correctCount = 0;

            Console.WriteLine("Performance test...");
            var sw = new Stopwatch();
            for (int i = 0; i < count; i++)
            {
                var capacity = random.Next(maxValue * vMax + 1);
                var profits = GenerateRandomArray(vMax, maxValue);
                var weights = GenerateRandomArray(vMax, maxValue);

                sw.Start();
                var actual = greedySolver.Solve(profits, weights, capacity);
                sw.Stop();

                var expected = bruteForceSolver.Solve(profits, weights, capacity);

                bool isCorrect = actual.SequenceEqual(expected);
                if (isCorrect)
                {
                    correctCount++;
                }

                Console.WriteLine($"Тест №{i}. Совпало с точным: {isCorrect}");

                GC.Collect();
            }
            Console.WriteLine("Процент совпадений решений: " + (double)correctCount / count);
            Console.WriteLine("Среднее время " + sw.Elapsed.TotalMilliseconds / count);
        }

        private static void Main(string[] args)
        {
            Solve(new BruteForceSolver(), "input.txt", "bruteForceSolver_output.txt");
            Solve(new BranchAndBoundSolver(), "input.txt", "branchAndBoundSolver_output.txt");
            Solve(new GreedySolver(), "input.txt", "greedySolver_output.txt");

            PerformanceTest();
        }

        private static void Solve(IKnapsackSolver solver, string inFileName, string outFileName)
        {
            int capacity;
            int[] profits, weights;
            using (StreamReader sr = new StreamReader(inFileName))
            {
                var splitLine = sr.ReadLine().Split(' ');
                var itemsCount = int.Parse(splitLine[0]);
                capacity = int.Parse(splitLine[1]);

                profits = new int[itemsCount];
                weights = new int[itemsCount];

                for (var i = 0; i < itemsCount; i++)
                {
                    splitLine = sr.ReadLine().Split(' ');
                    profits[i] = int.Parse(splitLine[0]);
                    weights[i] = int.Parse(splitLine[1]);
                }
            }

            var answer = solver.Solve(profits, weights, capacity);
            using (var sw = new StreamWriter(outFileName))
            {
                foreach (var item in answer)
                {
                    sw.WriteLine(item);
                }
            }
        }
    }
}
